package dateutils

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

// GetNow() returns time.Time.UTC
func GetNow() time.Time {
	return time.Now().UTC()
}

// GetNowString() returns time.Time in "2006-01-02T15:04:05.000+0000" format
func GetNowString() string {
	return GetNow().Format(apiIsoDateLayout)

}

// GetNowString() returns time.Time ("2006-01-02T15:04:05.000+0000") in "20060102150405" format
func GetNowYYYYMMDDTHHMMSS() string {
	timeStp := time.Now().UTC().String()
	timeStp = strings.Replace(timeStp, "-", "", -1)
	timeStp = strings.Replace(timeStp, " ", "T", 1)
	timeStp = strings.Replace(timeStp, " ", "", -1)
	timeStp = strings.Replace(timeStp, ":", "", -1)
	timeStp = strings.Split(timeStp, ".")[0]
	return timeStp
}

// StrZeroToTen() returns string with "0" before number if less than 10
// e.g. "5" returns "05"
func StrZeroToTen(m string) string {

	var list = []string{"1", "2", "3", "4", "5", "6", "7", "8", "9"}

	for _, b := range list {
		if b == m {
			m = fmt.Sprintf("0%v", m)
			break
		}
	}
	return m
}

// SortHour() returns a value (string) and and error (string)
// Used to check hour is 2 characters
func SortHour(s string, errorStr string) (string, string) {
	if s == "" {
		errorStr = strConcat(errorStr, "hour is blank")
	} else {
		if len(s) > 2 {
			errorStr = strConcat(errorStr, "hour length must be less than 3 digits long")
		} else {
			hourNInt, err := strconv.Atoi(StrZeroToTen(s))
			if err != nil {
				errorStr = strConcat(errorStr, "hour could not be converted to a number")
			} else {
				if hourNInt > 23 || hourNInt < 0 {
					errorStr = strConcat(errorStr, "hour should be between 0 and 23")
				} else {
					s = StrZeroToTen(s)
				}
			}
		}
	}
	return s, errorStr
}

// SortMin() returns a value (string) and and error (string)
// Used to check min is 2 characters
func SortMin(s string, errorStr string) (string, string) {
	if s == "" {
		errorStr = strConcat(errorStr, "min is blank")
	} else {
		if len(s) > 2 {
			errorStr = strConcat(errorStr, "min length must be less than 3 digits long")
		} else {
			minNInt, err := strconv.Atoi(StrZeroToTen(s))
			if err != nil {
				errorStr = strConcat(errorStr, "min could not be converted to a number")
			} else {
				if minNInt > 59 || minNInt < 0 {
					errorStr = strConcat(errorStr, "min should be between 0 and 23")
				} else {
					s = StrZeroToTen(s)
				}
			}
		}
	}
	return s, errorStr
}

// SortSec() returns a value (string) and and error (string)
// Used to check hour is 2 characters
func SortSec(s string, errorStr string) (string, string) {
	if s == "" {
		errorStr = strConcat(errorStr, "sec is blank")
	} else {
		if len(s) > 2 {
			errorStr = strConcat(errorStr, "sec length must be less than 3 digits long")
		} else {
			secNInt, err := strconv.Atoi(StrZeroToTen(s))
			if err != nil {
				errorStr = strConcat(errorStr, "sec could not be converted to a number")
			} else {
				if secNInt > 59 || secNInt < 0 {
					errorStr = strConcat(errorStr, "sec should be between 0 and 23")
				} else {
					s = StrZeroToTen(s)
				}
			}
		}
	}
	return s, errorStr
}

// returns timeStamp, converts year, month, day, hour, min, sec to concat string.
// checks
// year is len=4 and between 2000 and 3000
// month is len=2 and between 1-12
// day is len=2  and between 1-31
// hour is len=2 and between 0-23
// min is len=2 and between 0-59
// sec is len=2 and between 0-59
func GetDateTimeStamp(year string, month string, day string, hour string, min string, sec string) (*string, *string) {
	errorStr := ""
	var yearN, monthN, dayN, hourN, minN, secN int64
	_, _, _, _, _ = monthN, dayN, hourN, minN, secN
	if year == "" {
		errorStr = strConcat(errorStr, "year is blank")
	} else {
		if len(year) < 4 || len(year) > 4 {
			errorStr = strConcat(errorStr, "year length must be 4 digits long")
		}

		yearNInt, err := strconv.Atoi(year)
		if err != nil {
			errorStr = strConcat(errorStr, "year cannot be converted to int")
		}
		yearN = int64(yearNInt)
		if yearN < 2000 || yearN > 3000 {
			errorStr = strConcat(errorStr, "year is less than 2000 or greater than 3000")
		}
	}
	if month == "" {
		errorStr = strConcat(errorStr, "month is blank")
	} else {
		if len(month) > 2 {
			errorStr = strConcat(errorStr, "month length must be less than 3 digits long")
		} else {
			monthNInt, err := strconv.Atoi(StrZeroToTen(month))
			if err != nil {
				errorStr = strConcat(errorStr, "month could not be converted to a number")
			} else {
				if monthNInt > 12 || monthNInt < 1 {
					errorStr = strConcat(errorStr, "month should be between 1 and 12")
				} else {
					month = StrZeroToTen(month)
				}
			}
		}
	}
	if day == "" {
		errorStr = strConcat(errorStr, "day is blank")
	} else {
		if len(day) > 2 {
			errorStr = strConcat(errorStr, "day length must be less than 3 digits long")
		} else {
			dayNInt, err := strconv.Atoi(StrZeroToTen(day))
			if err != nil {
				errorStr = strConcat(errorStr, "day could not be converted to a number")
			} else {
				if dayNInt > 31 || dayNInt < 1 {
					errorStr = strConcat(errorStr, "day should be between 1 and 31")
				} else {
					day = StrZeroToTen(day)
				}
			}
		}
	}
	if errorStr != "" {
		return nil, &errorStr
	}
	hour, errorStr = SortHour(hour, errorStr)
	min, errorStr = SortMin(min, errorStr)
	sec, errorStr = SortSec(sec, errorStr)

	if errorStr != "" {
		return nil, &errorStr
	}
	retVal := fmt.Sprintf("%v-%v-%v %v:%v:%v", year, month, day, hour, min, sec)
	return &retVal, nil
}

// returns timeStamp, converts year, month, day, hour, min, sec to concat string.
// checks
// year is len=4 and between 2000 and 3000
// month is len=2 and between 1-12
// day is len=2  and between 1-31
func GetDate(year string, month string, day string) (*string, *string) {
	errorStr := ""

	if year == "" {
		errorStr = strConcat(errorStr, "year is blank")
	} else {
		if len(year) < 4 || len(year) > 4 {
			errorStr = strConcat(errorStr, "year length must be 4 digits long")
		}

		yearN, err := strconv.Atoi(year)
		if err != nil {
			errorStr = strConcat(errorStr, "year cannot be converted to int")
		}

		if yearN < 2000 || yearN > 3000 {
			errorStr = strConcat(errorStr, "year is less than 2000 or greater than 3000")
		}
	}
	if month == "" {
		errorStr = strConcat(errorStr, "month is blank")
	} else {
		if len(month) > 2 {
			errorStr = strConcat(errorStr, "month length must be less than 3 digits long")
		} else {
			monthNInt, err := strconv.Atoi(StrZeroToTen(month))
			if err != nil {
				errorStr = strConcat(errorStr, "month could not be converted to a number")
			} else {
				if monthNInt > 12 || monthNInt < 1 {
					errorStr = strConcat(errorStr, "month should be between 1 and 12")
				} else {
					month = StrZeroToTen(month)
				}
			}
		}
	}
	if day == "" {
		errorStr = strConcat(errorStr, "day is blank")
	} else {
		if len(day) > 2 {
			errorStr = strConcat(errorStr, "day length must be less than 3 digits long")
		} else {
			dayNInt, err := strconv.Atoi(StrZeroToTen(day))
			if err != nil {
				errorStr = strConcat(errorStr, "day could not be converted to a number")
			} else {
				if dayNInt > 31 || dayNInt < 1 {
					errorStr = strConcat(errorStr, "day should be between 1 and 31")
				} else {
					day = StrZeroToTen(day)
				}
			}
		}
	}
	if errorStr != "" {
		return nil, &errorStr
	}
	retVal := fmt.Sprintf("%v-%v-%v", year, month, day)
	return &retVal, nil
}
