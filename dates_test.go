package dateutils

import (
	"fmt"
	"reflect"
	"regexp"
	"strings"
	"testing"

	"github.com/taxedio/overthere"
)

const (
	dateRegex            = `\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}\+\d{4}UTC`
	stringRegex          = `\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}\+\d{4}`
	yyyyMMDDTHHMMSSRegex = `\d{8}T\d{6}`
)

func TestGetNow(t *testing.T) {
	tests := []struct {
		name string
		exp  string
		want bool
	}{
		{"test GetNow", dateRegex, true}, // test is against YYYY-MM-DDHHMM+0000UTC because milliseconds are recorded differently in Docker/Linux
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := GetNow()

			timeStr := got.String()
			timeSpcSlice := strings.Split(timeStr, " ")
			timeDotSlice := strings.Split(timeStr, ".")
			timeStr = fmt.Sprintf("%v%v%v", timeDotSlice[0], timeSpcSlice[2], timeSpcSlice[3])

			match, _ := regexp.MatchString(tt.exp, timeStr)
			if !match {
				t.Errorf("GetNow() = %v [string: %v], want %v", match, timeStr, tt.want)
			}
		})
	}
}

func TestGetNowString(t *testing.T) {
	tests := []struct {
		name string
		exp  string
		want bool
	}{
		{"test GetNow", stringRegex, true}, // updated Regex
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := GetNowString()
			match, _ := regexp.MatchString(tt.exp, got)
			if !match {
				t.Errorf("GetNow() = %v [string: %v], want %v", match, got, tt.want)
			}
		})
	}
}

func TestStrZeroToTen(t *testing.T) {
	type args struct {
		m string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"Test StrZeroToTen 0", args{m: "0"}, "0"},
		{"Test StrZeroToTen 1", args{m: "1"}, "01"},
		{"Test StrZeroToTen 2", args{m: "2"}, "02"},
		{"Test StrZeroToTen 3", args{m: "3"}, "03"},
		{"Test StrZeroToTen 4", args{m: "4"}, "04"},
		{"Test StrZeroToTen 5", args{m: "5"}, "05"},
		{"Test StrZeroToTen 6", args{m: "6"}, "06"},
		{"Test StrZeroToTen 7", args{m: "7"}, "07"},
		{"Test StrZeroToTen 8", args{m: "8"}, "08"},
		{"Test StrZeroToTen 9", args{m: "9"}, "09"},
		{"Test StrZeroToTen 10", args{m: "10"}, "10"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := StrZeroToTen(tt.args.m); got != tt.want {
				t.Errorf("StrZeroToTen() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSortHour(t *testing.T) {
	type args struct {
		s        string
		errorStr string
	}
	tests := []struct {
		name  string
		args  args
		want  string
		want1 string
	}{
		{"Test SortHour Blank", args{s: "", errorStr: ""}, "", "hour is blank"},
		{"Test SortHour 3 chars", args{s: "000", errorStr: ""}, "000", "hour length must be less than 3 digits long"},
		{"Test SortHour 01", args{s: "01", errorStr: ""}, "01", ""},
		{"Test SortHour spec char", args{s: "0!", errorStr: ""}, "0!", "hour could not be converted to a number"},
		{"Test SortHour spec char", args{s: "-1", errorStr: ""}, "-1", "hour should be between 0 and 23"},
		{"Test SortHour spec char", args{s: "24", errorStr: ""}, "24", "hour should be between 0 and 23"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := SortHour(tt.args.s, tt.args.errorStr)
			if got != tt.want {
				t.Errorf("SortHour() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("SortHour() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestSortMin(t *testing.T) {
	type args struct {
		s        string
		errorStr string
	}
	tests := []struct {
		name  string
		args  args
		want  string
		want1 string
	}{
		{"Test SortMin Blank", args{s: "", errorStr: ""}, "", "min is blank"},
		{"Test SortMin 3 chars", args{s: "000", errorStr: ""}, "000", "min length must be less than 3 digits long"},
		{"Test SortMin 01", args{s: "01", errorStr: ""}, "01", ""},
		{"Test SortMin spec char", args{s: "0!", errorStr: ""}, "0!", "min could not be converted to a number"},
		{"Test SortMin spec char", args{s: "-1", errorStr: ""}, "-1", "min should be between 0 and 23"},
		{"Test SortMin spec char", args{s: "60", errorStr: ""}, "60", "min should be between 0 and 23"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := SortMin(tt.args.s, tt.args.errorStr)
			if got != tt.want {
				t.Errorf("SortMin() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("SortMin() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestSortSec(t *testing.T) {
	type args struct {
		s        string
		errorStr string
	}
	tests := []struct {
		name  string
		args  args
		want  string
		want1 string
	}{
		{"Test SortSec Blank", args{s: "", errorStr: ""}, "", "sec is blank"},
		{"Test SortSec 3 chars", args{s: "000", errorStr: ""}, "000", "sec length must be less than 3 digits long"},
		{"Test SortSec 01", args{s: "01", errorStr: ""}, "01", ""},
		{"Test SortSec spec char", args{s: "0!", errorStr: ""}, "0!", "sec could not be converted to a number"},
		{"Test SortSec spec char", args{s: "-1", errorStr: ""}, "-1", "sec should be between 0 and 23"},
		{"Test SortSec spec char", args{s: "60", errorStr: ""}, "60", "sec should be between 0 and 23"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := SortSec(tt.args.s, tt.args.errorStr)
			if got != tt.want {
				t.Errorf("SortSec() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("SortSec() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestGetDateTimeStamp(t *testing.T) {
	type args struct {
		year  string
		month string
		day   string
		hour  string
		min   string
		sec   string
	}
	// TEst 1
	var (
		fullMsg      = "year is blank | month is blank | day is blank"
		yearMsg      = "month is blank | day is blank"
		monthMsg     = "year is blank | day is blank"
		dayMsg       = "year is blank | month is blank"
		yearTooLong  = "year length must be 4 digits long | year is less than 2000 or greater than 3000"
		yearString   = "year length must be 4 digits long | year cannot be converted to int | year is less than 2000 or greater than 3000"
		monthTooLong = "month length must be less than 3 digits long"
		monthString  = "month could not be converted to a number"
		monthThrtn   = "month should be between 1 and 12"
		dayString    = "day could not be converted to a number"
		dayThrTwo    = "day should be between 1 and 31"
		secFail      = "min should be between 0 and 23"
		dayToolong   = "day length must be less than 3 digits long"
	)
	tests := []struct {
		name  string
		args  args
		want  *string
		want1 *string
	}{
		{
			"Test GetDateTimeStamp() 1 - all blank",
			args{year: "", month: "", day: "", hour: "", min: "", sec: ""},
			nil,
			&fullMsg,
		},
		{
			"Test GetDateTimeStamp() 2 - just year",
			args{year: "2000", month: "", day: "", hour: "", min: "", sec: ""},
			nil,
			&yearMsg,
		},
		{
			"Test GetDateTimeStamp() 3 - just month",
			args{year: "", month: "01", day: "", hour: "", min: "", sec: ""},
			nil,
			&monthMsg,
		},
		{
			"Test GetDateTimeStamp() 4 - just day",
			args{year: "", month: "", day: "1", hour: "", min: "", sec: ""},
			nil,
			&dayMsg,
		},
		{
			"Test GetDateTimeStamp() 5 - year too long",
			args{year: "20005", month: "1", day: "1", hour: "", min: "", sec: ""},
			nil,
			&yearTooLong,
		},
		{
			"Test GetDateTimeStamp() 6 - year not a number",
			args{year: "a", month: "1", day: "1", hour: "", min: "", sec: ""},
			nil,
			&yearString,
		},
		{
			"Test GetDateTimeStamp() 7 - month too long",
			args{year: "2021", month: "123", day: "1", hour: "", min: "", sec: ""},
			nil,
			&monthTooLong,
		},
		{
			"Test GetDateTimeStamp() 8 - month is string",
			args{year: "2021", month: "aa", day: "1", hour: "", min: "", sec: ""},
			nil,
			&monthString,
		},
		{
			"Test GetDateTimeStamp() 9 - month is 13",
			args{year: "2021", month: "13", day: "1", hour: "", min: "", sec: ""},
			nil,
			&monthThrtn,
		},
		{
			"Test GetDateTimeStamp() 10 - day is string",
			args{year: "2021", month: "12", day: "aa", hour: "", min: "", sec: ""},
			nil,
			&dayString,
		},
		{
			"Test GetDateTimeStamp() 11 - day is 32",
			args{year: "2021", month: "12", day: "32", hour: "", min: "", sec: ""},
			nil,
			&dayThrTwo,
		},
		{
			"Test GetDateTimeStamp() 12 - okay",
			args{year: "2021", month: "12", day: "31", hour: "23", min: "59", sec: "59"},
			overthere.StrPointer("2021-12-31 23:59:59"),
			nil,
		},
		{
			"Test GetDateTimeStamp() 13 - second to trigger errorStr",
			args{year: "2021", month: "12", day: "31", hour: "23", min: "60", sec: "00"},
			nil,
			&secFail,
		},
		{
			"Test GetDateTimeStamp() 13 - day is too long",
			args{year: "2021", month: "12", day: "aaa", hour: "", min: "", sec: ""},
			nil,
			&dayToolong,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := GetDateTimeStamp(tt.args.year, tt.args.month, tt.args.day, tt.args.hour, tt.args.min, tt.args.sec)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetDateTimeStamp() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("GetDateTimeStamp() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestGetDate(t *testing.T) {
	type args struct {
		year  string
		month string
		day   string
	}
	var (
		blankAll  = "year is blank | month is blank | day is blank"
		longYear  = "year length must be 4 digits long | year is less than 2000 or greater than 3000"
		strYear   = "year cannot be converted to int | year is less than 2000 or greater than 3000"
		longMonth = "month length must be less than 3 digits long"
		strMonth  = "month could not be converted to a number"
		maxMonth  = "month should be between 1 and 12"
		longDay   = "day length must be less than 3 digits long"
		strDay    = "day could not be converted to a number"
		maxDay    = "day should be between 1 and 31"
	)
	tests := []struct {
		name  string
		args  args
		want  *string
		want1 *string
	}{
		{
			"Test GetDateTimeStamp() okay",
			args{year: "2021", month: "12", day: "31"},
			overthere.StrPointer("2021-12-31"),
			nil,
		},
		{
			"Test GetDateTimeStamp() blank all",
			args{year: "", month: "", day: ""},
			nil,
			&blankAll,
		},
		{
			"Test GetDateTimeStamp() year is long ",
			args{year: "20005", month: "12", day: "31"},
			nil,
			&longYear,
		},
		{
			"Test GetDateTimeStamp() year is string",
			args{year: "aaaa", month: "12", day: "31"},
			nil,
			&strYear,
		},
		{
			"Test GetDateTimeStamp() month is 3 digits",
			args{year: "2000", month: "123", day: "31"},
			nil,
			&longMonth,
		},
		{
			"Test GetDateTimeStamp() month is string",
			args{year: "2000", month: "aa", day: "31"},
			nil,
			&strMonth,
		},
		{
			"Test GetDateTimeStamp() month is 13",
			args{year: "2000", month: "13", day: "31"},
			nil,
			&maxMonth,
		},
		{
			"Test GetDateTimeStamp() day is 3 digits",
			args{year: "2000", month: "12", day: "311"},
			nil,
			&longDay,
		},
		{
			"Test GetDateTimeStamp() day is string",
			args{year: "2000", month: "12", day: "aa"},
			nil,
			&strDay,
		},
		{
			"Test GetDateTimeStamp() day is 32",
			args{year: "2000", month: "12", day: "32"},
			nil,
			&maxDay,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := GetDate(tt.args.year, tt.args.month, tt.args.day)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetDate() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("GetDate() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestGetNowYYYYMMDDTHHMMSS(t *testing.T) {
	tests := []struct {
		name string
		exp  string
		want bool
	}{
		{"test GetNow", yyyyMMDDTHHMMSSRegex, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := GetNowYYYYMMDDTHHMMSS()
			match, _ := regexp.MatchString(tt.exp, got)
			if !match && len(got) == len("YYYYMMDDTHHMMSS") {
				t.Errorf("GetNowYYYYMMDDTHHMMSS() = %v [string: %v], want %v", match, got, tt.want)
			}
		})
	}
}
