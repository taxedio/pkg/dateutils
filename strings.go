package dateutils

import "fmt"

// StrConcat, concats two string fields with separator "|"
func strConcat(s string, ns string) string {
	if s == "" {
		s = ns
	} else {
		s += fmt.Sprintf(" | %v", ns)
	}
	return s
}
