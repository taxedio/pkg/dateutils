package dateutils

import "testing"

func Test_strConcat(t *testing.T) {
	type args struct {
		s  string
		ns string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			"Test StrConcat 1",
			args{s: "", ns: ""},
			"",
		},
		{
			"Test StrConcat 2",
			args{s: "hour is blank", ns: "hour is blank"},
			"hour is blank | hour is blank",
		},
		{
			"Test StrConcat 3",
			args{s: "hour is blank | hour is blank", ns: "hour is blank"},
			"hour is blank | hour is blank | hour is blank",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := strConcat(tt.args.s, tt.args.ns); got != tt.want {
				t.Errorf("StrConcat() = %v, want %v", got, tt.want)
			}
		})
	}
}
